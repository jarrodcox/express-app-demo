FROM node:fermium-alpine3.14

ARG PORT=3000

WORKDIR /src/usr/app

COPY package.json server.js index.html info.html ./

RUN npm install

EXPOSE 3000

ENTRYPOINT [ "node", "server.js" ]
